#-*- coding: utf-8 -*-


import urllib
# import urllib2
import sys
import re
import mechanize
import cookielib


class Bot:
    def __init__(self):

        self.login_url = r'https://faberlic.by/index.php?option=com_user&view=login&lang=ru'
        self.order_url = 'https://faberlic.by/index2.php?'
        self.username = ''
        self.password = ''
        self.available_items = {}
        self.available_items_count = {}
        self.available_items_page = ''
        self.items_to_order = {}
        self.request_parameters = {'option': 'com_order',
                                   'view': 'editgood',
                                   'orderid': '',
                                   'idgood': '',
                                   'Itemid': '',
                                   'no_html': '1'}
        self.browser = mechanize.Browser()
        self.cj = cookielib.LWPCookieJar()
        self.browser.set_cookiejar(self.cj)
        self.browser.set_handle_equiv(True)
        # self.browser.set_handle_gzip(True)
        self.browser.set_handle_redirect(True)
        self.browser.set_handle_referer(True)
        self.browser.set_handle_robots(False)
        self.browser.addheaders = [('User-agent',
                        'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0')]

    # loads from file items to order and their counts
    def parse_input_files(self):
        items_input_file = open('data.txt', 'r')
        for line in items_input_file:
            if not (line == '\n' or line == ' ' or line == '\r\n'):
                item, count = line.split()
                self.items_to_order[item] = count
        items_input_file.close()
        user_data_input_file = open('user.txt', 'r')
        line = user_data_input_file.readline()
        self.username, self.password = line.split()
        print self.username, self.password
        # sys.exit(0)

    # saves responses to file (for debug)
    def save_to_file(self, page, filename):
        output_file = open(filename, 'w')
        output_file.write(page)
        output_file.close()
        print '---', filename, ' created'

    # login user
    def login(self):
        print '...Try to log in...'
        self.browser.open(self.login_url)
        self.browser.select_form(name = "com-login")
        self.browser['username'] = self.username
        self.browser['passwd'] = self.password
        response = self.browser.submit()
        kabinet_page = response.read()
        match = re.search(r'Ошибка авторизации', kabinet_page)
        if match:
            print "Login failed"
            sys.exit(1)
        else:
            print "Login successful"
        #save_to_file(kabinet_page, 'kabinet.html')

    def go_to_order_page(self):
        response = self.browser.follow_link(text_regex='Быстрый заказ')
        zakaz_url = response.geturl()
        match = re.search(r'orderid=(\d+)',zakaz_url)
        if match:
            orderid = match.group(1)
            self.request_parameters['orderid'] = orderid
            #print 'Order id OK'
        else:
            print "Can't find Order id"
            raw_input()
            sys.exit(1)
        self.available_items_page = response.read()

    # find available items in page and saves it to dictionary
    def parse_items(self, page):
        # match = re.findall(r'{ id:(\d+),\sart:\s"(\d+).+realcount:\s(\d+)"', page)
        match = re.findall(r'{ id:(\d+),\sart:\s"(\d+).+realcount:\s(\d+)', page)
        if match:
            for line in match:
                self.available_items[line[1]] = line[0]
                self.available_items_count[line[1]] = line[2]
        else:
            print "Can't find Items"

    # order specified items
    def order_items(self):

        while len(self.items_to_order) > 0:

            for key in self.items_to_order.keys():
              #print 'key:', key, 'avail count:', AVAILABLE_ITEMS_COUNT[key], 'order count:', ITEMS_TO_ORDER[key]
                if key in self.available_items_count:
                    if int(self.available_items_count[key]) < int(self.items_to_order[key]) \
                            and int(self.available_items_count[key]) > 0:
                        self.items_to_order[key] = self.available_items_count[key]

                if key in self.available_items and \
                        (int(self.available_items_count[key]) >= int(self.items_to_order[key])):
                    self.request_parameters['idgood'] = self.available_items[key]
                    url =self.order_url + urllib.urlencode(self.request_parameters)
                    #print url
                    post_data = '&countgood=' + str(self.items_to_order[key])
                    self.browser.open_novisit(url, post_data)
                    print key, self.items_to_order[key], ' OK'
                    del self.items_to_order[key]
                    # print '----', ITEMS_TO_ORDER

            if len(self.items_to_order)==0:
                print 'VSE ZAKAZANO'
                flag = raw_input()
                sys.exit(0)

            else:
                response = self.browser.reload()
                self.available_items_page = response.read()
                #save_to_file(zakaz_page, 'reload.html')
                self.parse_items(self.available_items_page)
                print 'reload page'
                #print 'Ostalos: ', ITEMS_TO_ORDER


def main():

    bot_obj = Bot()
    bot_obj.parse_input_files()
    bot_obj.login()
    bot_obj.go_to_order_page()
    bot_obj.parse_items(bot_obj.available_items_page)
    bot_obj.order_items()

    sys.exit(0)


if __name__ == "__main__":
    main()
